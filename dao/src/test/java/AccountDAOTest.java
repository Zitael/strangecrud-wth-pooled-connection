import model.Account;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AccountDAOTest {
    private AccountDAO subj;

    @Before
    public void setUp() {
        System.setProperty("jdbcUrl", "jdbc:postgresql://localhost:5432/accounts");//"jdbc:h2:mem:test_mem");
        System.setProperty("username", "me");
        System.setProperty("password", "me");
        System.setProperty("databaseChangeLog", "databaseChangeLog_test.xml");
        subj = DaoFactory.getAccountDAO();
    }


    @Test
    public void findById() {
        Account acc = subj.findById(1L);
        assertEquals(acc.getLogin(), "TestLogin1");
        assertEquals(acc.getName(), "TestName1");
        assertEquals(acc.getPassword(), "TestPassword1");
    }

    @Test
    public void findByEmail() {
        Account acc = subj.findByEmail("TestLogin1");
        assertEquals(acc.getId(), 1L);
        assertEquals(acc.getName(), "TestName1");
        assertEquals(acc.getPassword(), "TestPassword1");
    }

    @Test
    public void insert() {
        Account acc = new Account();
        acc.setLogin("InsertedLogin");
        acc.setName("InsertedName");
        acc.setPassword("InsertedPassword");
        subj.insert(acc);
        assertNotEquals(acc.getId(), 0);
    }

    @Test
    public void update() {
        Account acc = new Account();
        acc.setName("updatedName");
        acc.setLogin("updatedLogin");
        acc.setPassword("updatedPassword");
        acc.setId(2L);
        subj.update(acc);
        Account acc2 = subj.findById(2L);
        assertEquals(acc.getName(), acc2.getName());
    }

    @Test
    public void delete() {
        subj.delete(3L);
        assertNull(subj.findById(3L));
    }
}