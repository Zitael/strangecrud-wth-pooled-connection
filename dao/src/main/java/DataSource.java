import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseConnection;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.postgresql.ds.PGConnectionPoolDataSource;

import javax.sql.PooledConnection;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {
    private static DataSource dataSource;
    private static PGConnectionPoolDataSource pGConnectionPoolDataSource;
    private static PooledConnection pooledConnection;

    private DataSource() {
        pGConnectionPoolDataSource = getPGConnectionPoolDataSource();
        try {
            pooledConnection = pGConnectionPoolDataSource.getPooledConnection();
        } catch (SQLException e) {
            throw new CustomException("Проблемы с подключением к БД, обратитесь к администратору", e);
        }
    }

    private static PGConnectionPoolDataSource getPGConnectionPoolDataSource() {
        if (pGConnectionPoolDataSource == null) {
            PGConnectionPoolDataSource temp = new PGConnectionPoolDataSource();
            temp.setUrl((System.getProperty("jdbcUrl", "jdbc:postgresql://localhost:5432/accounts")));
            temp.setUser(System.getProperty("username", "postgres"));
            temp.setPassword(System.getProperty("username", "zee"));
            pGConnectionPoolDataSource = temp;
            initDataBase(getDataSource());
        }
        return pGConnectionPoolDataSource;
    }
    private static void initDataBase(DataSource dataSource) {
        try {
            DatabaseConnection connection = new JdbcConnection((dataSource.getConnection()));
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(connection);
            Liquibase liquibase = new Liquibase(System.getProperty("databaseChangeLog", "databaseChangeLog.xml"),
                    new ClassLoaderResourceAccessor(),
                    database);
            liquibase.update(new Contexts());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public Connection getConnection() throws SQLException {
        return pooledConnection.getConnection();
    }

    public static DataSource getDataSource() {
        if (dataSource == null) {
            dataSource = new DataSource();
        }
        return dataSource;
    }
}
