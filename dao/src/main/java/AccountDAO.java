import model.Account;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountDAO implements Dao<Account, Long> {
    private DataSource dataSource;

    public AccountDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private Account getFoundUserByResultSet(ResultSet rs) {
        if (rs != null) {
            Account acc = new Account();
            try {
                while (rs.next()) {
                    acc.setId(rs.getLong("id"));
                    acc.setLogin(rs.getString("login"));
                    acc.setPassword(rs.getString("password"));
                    acc.setName(rs.getString("name"));
                }
                return acc;
            } catch (SQLException e) {
                throw new CustomException("Проблемы при подключении к БД. Обратитесь к администратору.", e);
            }
        }
        return null;
    }

    public Account findById(Long id) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps;
            String query = "SELECT * FROM account WHERE id = ?";
            ResultSet rs = null;
            if (conn != null) {
                ps = conn.prepareStatement(query);
                ps.setLong(1, id);
                rs = ps.executeQuery();
            }
            if (rs != null) {
                return getFoundUserByResultSet(rs);
            }
        } catch (SQLException e) {
            throw new CustomException("Проблемы при подключении к БД. Обратитесь к администратору.", e);
        }
        return null;
    }

    public Account findByEmail(String email) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps;
            String query = "SELECT * FROM account WHERE login = ?";
            ResultSet rs = null;
            if (conn != null) {
                ps = conn.prepareStatement(query);
                ps.setString(1, email);
                rs = ps.executeQuery();
            }
            if (rs != null) {
                return getFoundUserByResultSet(rs);
            }
        } catch (SQLException e) {
            throw new CustomException("Проблемы при подключении к БД. Обратитесь к администратору.", e);
        }
        return null;
    }

    public void insert(Account acc) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps;
            String query = "INSERT INTO account (login, password, name) VALUES (?,?,?)";
            if (conn != null) {
                ps = conn.prepareStatement(query);
                ps.setString(1, acc.getLogin());
                ps.setString(2, acc.getPassword());
                ps.setString(3, acc.getName());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new CustomException("Проблемы при подключении к БД. Обратитесь к администратору.", e);
        }
    }

    public void update(Account acc) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps;
            String query = "UPDATE account SET login = ?, password = ?, name = ? WHERE id = ?";
            if (conn != null) {
                ps = conn.prepareStatement(query);
                ps.setString(1, acc.getLogin());
                ps.setString(2, acc.getPassword());
                ps.setString(3, acc.getName());
                ps.setLong(4, acc.getId());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new CustomException("Проблемы при подключении к БД. Обратитесь к администратору.", e);
        }
    }

    public void delete(Long id) {
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps;
            String query = "DELETE FROM account WHERE id = ?";
            if (conn != null) {
                ps = conn.prepareStatement(query);
                ps.setLong(1, id);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new CustomException("Проблемы при подключении к БД. Обратитесь к администратору.", e);
        }
    }
}
