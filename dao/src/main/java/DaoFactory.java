public class DaoFactory {
    private DaoFactory(){}
    private static AccountDAO accountDAO;
    public static AccountDAO getAccountDAO(){
        if (accountDAO == null){
            accountDAO = new AccountDAO(DataSource.getDataSource());
        }
        return accountDAO;
    }
}
