public class CustomException extends RuntimeException {
    public CustomException(String message, Throwable t) {
        super(message);
    }
}

