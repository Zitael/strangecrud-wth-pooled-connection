public interface Dao <DOMAIN, ID>{
        DOMAIN findById(ID id);
        void insert(DOMAIN domain);
        void update(DOMAIN domain);
        void delete(ID id);
}
