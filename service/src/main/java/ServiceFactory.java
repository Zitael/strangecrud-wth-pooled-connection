public class ServiceFactory {
    private ServiceFactory(){}
    private static AccountService accountService;
    public static AccountService getAccountService(){
        if (accountService == null){
            accountService = new AccountService();
        }
        return accountService;
    }
}
