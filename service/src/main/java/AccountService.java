import dto.AccountDTO;
import model.Account;

public class AccountService implements Service<AccountDTO, Long> {
    private final AccountDAO accountDAO;
    private final DtoModelConverter converter;

    public AccountService() {
        this.accountDAO = DaoFactory.getAccountDAO();
        this.converter = new DtoModelConverter();
    }

    public AccountDTO findById(Long id) throws CustomException {
        Account acc = accountDAO.findById(id);
        if (acc != null) {
            return converter.convertModelToDto(acc);
        }
        return null;
    }
    public AccountDTO findByEmail(String email){
        Account acc = accountDAO.findByEmail(email);
        if (acc != null){
            return converter.convertModelToDto(acc);
        }
        return null;
    }

    public void insert(AccountDTO accountDTO) {
        accountDAO.insert(converter.convertDtoToModel(accountDTO));
    }

    public void update(AccountDTO accountDTO) {
        accountDAO.update(converter.convertDtoToModel(accountDTO));
    }

    public void delete(Long id) {
        accountDAO.delete(id);
    }

    private class DtoModelConverter {
        Account convertDtoToModel(AccountDTO accountDTO) {
            Account acc = new Account();
            acc.setId(accountDTO.getId());
            acc.setLogin(accountDTO.getLogin());
            acc.setName(accountDTO.getName());
            acc.setPassword(accountDTO.getPassword());
            return acc;

        }

        AccountDTO convertModelToDto(Account acc) {
            AccountDTO accountDTO = new AccountDTO();
            accountDTO.setId(acc.getId());
            accountDTO.setName(acc.getName());
            accountDTO.setLogin(acc.getLogin());
            accountDTO.setPassword(acc.getPassword());
            return accountDTO;
        }
    }
}
