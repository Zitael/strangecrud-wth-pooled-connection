import dto.AccountDTO;

import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        String login;
        String password;
        String name;
        login = "123";
        password = "456";
        name = "789";
//        Scanner in = new Scanner(System.in);
//        System.out.println("Введите логин:");
//        login = in.next();
//        System.out.println("Введите логин:");
//        password = in.next();
//        System.out.println("Введите логин:");
//        name = in.next();


        System.out.println("Начало, добавление в базу");
        AccountService service = ServiceFactory.getAccountService();

        AccountDTO acc = new AccountDTO();
        acc.setLogin(login);
        acc.setPassword(password);
        acc.setName(name);
        service.insert(acc);
        System.out.println("========================");
        System.out.println("Чтение из базы:");
        AccountDTO accountDTO = service.findByEmail(login);
        if (accountDTO != null) {
            System.out.println("Здравствуйте, " + accountDTO.getName());
            System.out.println("Ваш id: " + accountDTO.getId());
            System.out.println("Ваш логин: " + accountDTO.getLogin());
            System.out.println("Ваш пароль: " + accountDTO.getPassword());
            System.out.println("==============================");
            System.out.println("Изменение имени:");
            accountDTO.setName("NotName");
            service.update(accountDTO);
            System.out.println("Новое имя в базе: "+ service.findById(accountDTO.getId()).getName());
            System.out.println("==============================");
            System.out.println("Удаление из базы, новый поиск:");
            service.delete(accountDTO.getId());
            System.out.println("Найдено: "+ service.findById(accountDTO.getId()).getName());
        } else System.out.println("Пользователь не найден");
    }
}
